package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Config struct {
	RRDUserName     string
	RRDPassword     string
	RRDUrl          string
	RRDLendContract string

	DbConn string

	S3Url         string
	S3UserName    string
	S3Password    string
	S3UploadUrl   string
	S3DownloadUrl string
}

func LoadCfg() *Config {

	cfg := Config{}
	bytes, err := ioutil.ReadFile("config.json")
	if err != nil {
		fmt.Println("读取 config.json 出错: ", err.Error())
		return nil
	}

	if err := json.Unmarshal(bytes, &cfg); err != nil {
		fmt.Println("json解析 出错: ", err.Error())
		return nil
	}

	fmt.Println("测试下读取到的config数据 dbconn: " + cfg.DbConn)
	return &cfg
}

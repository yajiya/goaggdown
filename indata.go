package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
)

type InItem struct {
	LoanId int
	Name   string
}

type InData struct {
	Type1 []InItem
	Type2 []InItem
	Type3 []InItem
}

func LoadInData() InData {
	raw, err := ioutil.ReadFile("./indata.json")
	if err != nil {
		fmt.Println("读取 indata.json 出错: ", err.Error())
		os.Exit(1)
	}

	tmpdata := InData{}
	err = json.Unmarshal(raw, &tmpdata)
	if err != nil {
		fmt.Println("解析 indata.json 出错: ", err.Error())
		os.Exit(1)
	}

	fmt.Println("测试下读取到的indata数据: type1长度" + strconv.Itoa(len(tmpdata.Type1)))

	return tmpdata
}

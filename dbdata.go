package main

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type Loan struct {
	Id         int `db:"id"`
	WithdrawId int `db:"withdraw_id"`
}

type BondOffer struct {
	Id                 int `db:"id"`
	MajorDepoProductId int `db:"major_depo_product_id"`
}

type LoanDocument struct {
	FileAddress  string `db:"file_address"`
	DocumentType int    `db:"document_type"`
}

func FetchLoan(loanId int) Loan {
	cfg := LoadCfg()
	db, err := sqlx.Open("mysql", cfg.DbConn)
	defer db.Close()
	loan := Loan{}

	err = db.Get(&loan, "SELECT id, withdraw_id FROM c_loan_basic where id=? LIMIT 1", loanId)
	if err != nil {
		fmt.Println("读取loan出错。" + err.Error())
	}
	return loan
}

func FetchBondOfferByWithdraw(withdrawId int) BondOffer {
	cfg := LoadCfg()
	db, err := sqlx.Open("mysql", cfg.DbConn)
	defer db.Close()

	bo := BondOffer{}
	err = db.Get(&bo,
		"SELECT id, major_depo_product_id FROM c_bond_offer where withdraw_id=? LIMIT 1",
		withdrawId)
	if err != nil {
		fmt.Println("读取bondoffer出错。" + err.Error())
	}
	return bo
}

func FetchLoanDocuments(withdrawId int) []LoanDocument {
	cfg := LoadCfg()
	db, err := sqlx.Open("mysql", cfg.DbConn)
	defer db.Close()

	lds := []LoanDocument{}
	err = db.Select(&lds,
		"SELECT file_address, document_type FROM c_loan_document WHERE withdraw_i? LIMIT 1",
		withdrawId)
	if err != nil {
		fmt.Println("读取loandocument出错。" + err.Error())
	}
	return lds
}

//func main() {
//	db, _ := sqlx.Open("mysql", "ecifceshi:ecifceshi123@tcp(10.255.33.35:3312)/abacustest")
//	loan := Loan{}
//	err := db.Get(&loan, "SELECT withdraw_id FROM c_loan_basic LIMIT 1")
//	if err != nil {
//		fmt.Println(err.Error())
//	}
//	fmt.Println(loan.WithdrawId)
//	fmt.Println("done")
//}
